/**
 * hashload
 */
import java.nio.ByteBuffer;
import java.io.IOException;
import java.io.FileNotFoundException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;

public class hashload implements dbimpl {
	public static void main(String[] args) {
		hashload load = new hashload();

		long startTime = System.currentTimeMillis();
		load.readArguments(args);
		long endTime = System.currentTimeMillis();

		System.out.println("Query time: " + (endTime - startTime) + "ms");
	}

	// reading command line arguments
	public void readArguments(String args[]) {
		if (args.length == 1) {
			if (isInteger(args[0])) { 
				readHeapWriteHash(Integer.parseInt(args[0])); 
			}
		} else {
		  System.out.println("Error: only pass in two arguments");
		}
	}

	// check if pagesize is a valid integer
	public boolean isInteger(String s) {
		boolean isValidInt = false;
	
		try {
			Integer.parseInt(s);
			isValidInt = true;
		} catch (NumberFormatException e) {
			e.printStackTrace();
		}
		
		return isValidInt;
	}

	public void readHeapWriteHash(int pagesize) {
		File heapfile = new File(HEAP_FNAME + pagesize);
		int intSize = 4;
		int pageCount = 0;
		int recCount = 0;
		int recordLen = 0;
		int rid = 0;
		boolean isNextPage = true;
		boolean isNextRecord = true;
		int overall = 0;

		//Use 4.1Mill
		byte[][] buckets = new byte[HASH_VALUE][];

		for(int i = 0; i < HASH_VALUE; i++) {
			buckets[i] = intToByteArray(-1);
		}

		System.out.println("Hashing the HeapFile");

		try {
			FileInputStream fis = new FileInputStream(heapfile);
			FileOutputStream fos = new FileOutputStream(HASH_FNAME + pagesize);
			// reading page by page
			while (isNextPage) {
				byte[] bPage = new byte[pagesize];
				byte[] bPageNum = new byte[intSize];
				fis.read(bPage, 0, pagesize);
				System.arraycopy(bPage, bPage.length-intSize, bPageNum, 0, intSize);

				// reading by record, return true to read the next record
				isNextRecord = true;
				while (isNextRecord) {
					byte[] bRecord = new byte[RECORD_SIZE];
					byte[] bRid = new byte[intSize];
					try {
						System.arraycopy(bPage, recordLen, bRecord, 0, RECORD_SIZE);
						System.arraycopy(bRecord, 0, bRid, 0, intSize);
						rid = ByteBuffer.wrap(bRid).getInt();
						if (rid != recCount)
						{
							isNextRecord = false;
						}
						else
						{
							//printRecord(bRecord, name);
							addHash(buckets, bRecord, recCount * RECORD_SIZE, pageCount * pagesize);
							overall++;
							recordLen += RECORD_SIZE;
						}
						recCount++;
						// if recordLen exceeds pagesize, catch this to reset to next page
					} catch (ArrayIndexOutOfBoundsException e) {
						isNextRecord = false;
						recordLen = 0;
						recCount = 0;
						rid = 0;
					}
				}
				// check to complete all pages
				if (ByteBuffer.wrap(bPageNum).getInt() != pageCount) {
					isNextPage = false;
				}
				pageCount++;
			}

			System.out.println("Writing to HashFile");

			for(int i = 0; i < HASH_VALUE; i++) {
				fos.write(buckets[i]);
			}

			System.out.println("Overall Hashes = " + overall);

			fos.close();
		} catch (FileNotFoundException e) {
			System.out.println("File: " + HEAP_FNAME + pagesize + " not found.");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void addHash(byte[][] buckets, byte[] rec, int recOffset, int pageOffset) {
		boolean collision = false;
		String record = new String(rec);
		String bnName = record.substring(RID_SIZE + REGISTER_NAME_SIZE, RID_SIZE + REGISTER_NAME_SIZE + BN_NAME_SIZE);

		int hashValue = Math.abs(bnName.hashCode() % HASH_VALUE);

		byte[] byteOffsets = intToByteArray(pageOffset + recOffset);
		//Handle collision
		if(byteArrayToInt(buckets[hashValue]) != -1) {
			collision = true;
		}

		while(collision) {
			hashValue++;

			if(hashValue > HASH_VALUE) {
				hashValue = 0;
			}

			if(byteArrayToInt(buckets[hashValue]) == -1) {
				collision = false;
			}
		}

		buckets[hashValue] = byteOffsets;
	}

	public byte[] intToByteArray(int i) {
	   ByteBuffer bBuffer = ByteBuffer.allocate(4);
	   bBuffer.putInt(i);
	   return bBuffer.array();
	}

	public int byteArrayToInt(byte[] b) {
		int value = 0;
		for (int i = 0; i < 4; i++) {
			int shift = (4 - 1 - i) * 8;
			value += (b[i] & 0x000000FF) << shift;
		}
		return value;
	}
}