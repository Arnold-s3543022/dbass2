import java.nio.ByteBuffer;
import java.io.IOException;
import java.io.FileNotFoundException;
import java.io.File;
import java.io.FileInputStream;
import java.io.RandomAccessFile;

/**
 *  Database Systems - HEAP IMPLEMENTATION
 */

public class hashquery implements dbimpl {
    // initialize
    public static void main(String args[]) {
        hashquery load = new hashquery();

        // calculate query time
        long startTime = System.currentTimeMillis();
        load.readArguments(args);
        long endTime = System.currentTimeMillis();

        System.out.println("Query time: " + (endTime - startTime) + "ms");
    }


    // reading command line arguments
    public void readArguments(String args[]) {
        if (args.length == 2) {
            if (isInteger(args[1])) {
                readHashReadHeap(args[0], Integer.parseInt(args[1]));
            }
        } else {
            System.out.println("Error: only pass in two arguments");
        }
    }

    // check if pagesize is a valid integer
    public boolean isInteger(String s) {
        boolean isValidInt = false;
        
        try {
            Integer.parseInt(s);
            isValidInt = true;
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }

        return isValidInt;
    }

    // read heapfile by page
    public void readHashReadHeap(String query, int pagesize) {
        File heapfile = new File(HEAP_FNAME + pagesize);
        File hashfile = new File(HASH_FNAME + pagesize);

        int intSize = 4;
        int pageCount = 0;
        int recCount = 0;
        int recordLen = 0;
        int rid = 0;
        boolean isNextPage = true;
        boolean isNextRecord = true; 
        
        byte[] DATA = null;
        try {
            DATA = new byte[BN_NAME_SIZE];
            byte[] DATA_SRC = query.trim().getBytes(ENCODING);
            if (query != "") {
                System.arraycopy(DATA_SRC, 0, DATA, 0, DATA_SRC.length);
            }
        } catch(Exception e) {

        }
        query = new String(DATA);

        try {
            RandomAccessFile heapRaf = new RandomAccessFile(heapfile, "r");
            RandomAccessFile hashRaf = new RandomAccessFile(hashfile, "r");

            int hashOffset = queryHashed(query);
            byte[] bRecOffset = new byte[intSize];

            hashRaf.seek(hashOffset * intSize);
            hashRaf.read(bRecOffset, 0, intSize);

            int recOffset = byteArrayToInt(bRecOffset);
            
            if(recOffset != -1) {
                System.out.println(byteArrayToInt(bRecOffset));

                byte[] bRecord = new byte[RECORD_SIZE];
                heapRaf.seek(recOffset);
                heapRaf.read(bRecord, 0, RECORD_SIZE);
                
                boolean check = queryMatch(bRecord, query);

                while(!check) {
                    hashOffset++;

                    if(hashOffset > HASH_VALUE) {
                        hashOffset = 0;
                    }

                    heapRaf.seek(0);
                    hashRaf.seek(hashOffset * intSize);
                    hashRaf.read(bRecOffset, 0, intSize);
                    recOffset = byteArrayToInt(bRecOffset);
                    // System.out.println(byteArrayToInt(bRecOffset));
                    if(recOffset != -1) {
                        heapRaf.seek(0);
                        heapRaf.seek(recOffset);
                        heapRaf.read(bRecord, 0, RECORD_SIZE);
                        check = queryMatch(bRecord, query);
                    }
                }
            }
        } catch (FileNotFoundException e) {
            System.out.println("File: " + HEAP_FNAME + pagesize + " not found.");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public int queryHashed(String query) {
        int hashValue = Math.abs(query.hashCode() % HASH_VALUE);
        return hashValue;
    }

    public int byteArrayToInt(byte[] b) {
		int value = 0;
		for (int i = 0; i < 4; i++) {
			int shift = (4 - 1 - i) * 8;
			value += (b[i] & 0x000000FF) << shift;
		}
		return value;
    }
    
    public boolean queryMatch(byte[] rec, String query) {
        String record = new String(rec);
        String bnName = record.substring(RID_SIZE+REGISTER_NAME_SIZE,
                            RID_SIZE+REGISTER_NAME_SIZE+BN_NAME_SIZE);
        if(query.equals(bnName)) {
            System.out.println("Found a match");
            return true;
        }

        System.out.println(query + " vs. " + bnName);
        return false;
    }

    // returns records containing the argument text from shell
    public void printRecord(byte[] rec, String input) {
        String record = new String(rec);
        String BN_NAME = record
                            .substring(RID_SIZE+REGISTER_NAME_SIZE,
                            RID_SIZE+REGISTER_NAME_SIZE+BN_NAME_SIZE);
        if (BN_NAME.toLowerCase().contains(input.toLowerCase())) {
            System.out.println(record);
        }
    }
}
